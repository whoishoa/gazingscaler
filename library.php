<?php

/*
 * File: library.php
 * Author: Hoa Mai
 * Date: June 22, 2013
 * =======================
 * Functions:
 * bool isAuthenticated() // returns $_SESSION["authenticated"]
 * void setAuthenticated( bool whetherAuthenticated )
 * string getUser() // returns $_SESSION["user"]
 * void setUser(string username)
 * string getUserItem(string user, string field)
 * void setUserItem(string user, string field, string value)
 * bool isEmptyString(string toCheck)
 * connect() // returns FALSE if unsuccessful
 * void setTableItem(string table, string primaryKey, string primaryKeyValue, string field, string value)
 * getTableItem(string table, string primaryKey, string primaryKeyValue, string field)
 * bool isTakenInTable(string table, string category, string value)
 * void addFiller(int width, int height)
 * string getBrowser()
 * string sqlhtmlsafe(string toMakeSafe) 
 * int getStockValue(string stockId)
 * char randHex() // random hex digit
 * string generateSessionId()
 * bool isValidUsername(string username)
 * void playSound(string soundLocation)
 * void setSound(string soundLocation)
 */

$_library = TRUE;

// Constants

$_padding = 4;
$_mazeHeight = 101;
$_mazeWidth = 201;
$_moveSpeed = '150';
$_slowSpeed = '600';

/*
 * Randomizes an hexidemical value
 */
function randHex(){
   $res = rand(1, 16);
   if ($res <= 6){
      return chr(96 + $res);
   } else {
      return $res - 7;
   }
}

/*
 * Generates a session id
 */
function generateSessionId(){
   $id = "";
   for ($i = 0; $i < 32; $i++){
      $id .= randHex();
   }
   return $id;
}

/*
 * Returns true if the username is longer than 6 characters,
 * starts with a letter and contains only letters, numbers,
 * and underscores. Otherwise, this returns false.
 */
function isValidUsername( $user ){
   if ( strlen($user) < 6 ){
      return FALSE;
   } else {
      if ( ctype_alpha(substr($user, 0, 1)) ){
         for ($i = 0; $i < strlen($user); $i++){
            $char = substr($user, $i, 1);
            if (!ctype_alnum($char)){
               if ($char != '_'){
                  return FALSE;
               }
            }
         }
         return TRUE;
      } else {
         return FALSE;
      }
   }
}

/*
 * Plays the sound file located at the String argument when the webpage loads.
 * This function can only be used once per webpage.
 */
function playSound($loc){
   echo "<script language='javascript' type='text/javascript'>";
   echo "QT_WriteOBJECT('$loc', '1', '1', '', 'EnableJavaScript', 'True', 'postdomevents', 'True', 'emb#NAME', 'mainsound', 'obj#id', 'mainsound', 'obj#name', 'mainsound', 'emb#id', 'qtmovie_embed');";
   echo "</script>";
}

/*
 * Sets this sound to be played when the Play() function is called in
 * javascript.
 */
function setSound($loc){
   echo "<script language='javascript' type='text/javascript'>";
   echo "QT_WriteOBJECT('$loc', '1', '1', '', 'EnableJavaScript', 'True', 'postdomevents', 'True', 'AUTOPLAY', 'False', 'emb#NAME', 'mainsound', 'obj#id', 'mainsound', 'obj#name', 'mainsound', 'emb#id', 'qtmovie_embed');";
   echo "</script>";
}

function getStockValue($stockId){
  $link = "http://download.finance.yahoo.com/d/quotes.csv?s=" . strtoupper($stockId) . "&f=l1&e=.csv";
  $csv = fopen($link, "r");
  $stockValue = fgetcsv($csv);
  return $stockValue[0];
}

function isAuthenticated(){
   return $_SESSION["authenticated"];
}

function setAuthenticated($toSet){
   $_SESSION["authenticated"] = $toSet;
}

function getUser(){
   return $_SESSION["user"];
}

function setUser($toSet){
   $_SESSION["user"] = $toSet;
}

function isEmptyString($string){
   if (strlen($string) > 0){
      return false;
   }
   return true;
}

function connect(){
   if (($connection = mysql_connect('localhost','root', 'Ameliorate23!')) == FALSE){
      echo "Connection failed";
      return FALSE;
   } else {
      if (mysql_select_db("new", $connection) == FALSE){
         echo "Could not connect to database";
         return FALSE;
      } else {
         return $connection;
      }
   }
}

function addTableItem($name, $row, $col, $src, $isUsed, $motion) {
   $sql = sprintf("INSERT INTO `new`.`items` (`name`, `row`, `col`,`src`,`isUsed`,`motion`) VALUES ('$name', $row, $col, '$src', $isUsed, '$motion');");
   $toReturn = mysql_query($sql);
   return $toReturn;
}

function getItemInfo($name,  $item) {
    return getTableItem('items', 'name', $name, $item);
}

function setItemInfo($name, $item, $value) {
    return setTableItem('items', 'name', $name, $item, $value);
}

function hasItemName($name) {
   $sql = sprintf("SELECT `name` FROM `items` WHERE `name` = '%s'", mysql_escape_string($name));
   $result = mysql_query($sql);
   $numRows =  mysql_num_rows($result);
   return $numRows != 0;
}

function setTableItem($table, $primaryKey, $primaryKeyValue, $item, $value){
   $sql = sprintf('UPDATE `new`.`%s` SET `%s` = \'%s\' WHERE CONVERT(`%s`.`%s` USING utf8) = \'%s\' LIMIT 1;', 
      $table, $item, mysql_real_escape_string($value), $table, $primaryKey, $primaryKeyValue);
   return mysql_query($sql);
}

function getTableItem($table, $primaryKey, $primaryKeyValue, $item){
   $sql = sprintf('SELECT * FROM %s WHERE `%s`=\'%s\';', $table, $primaryKey, mysql_real_escape_string($primaryKeyValue));
   $result = mysql_query($sql);
   if ($result == FALSE){
      echo "Could not access $item on $primaryKey:$primaryKeyValue in table $table<br />";
      return "";
   } 
   $row = mysql_fetch_assoc($result);
   return $row[$item];
}

function setUserItem($user, $item, $value){
   $user = strtolower($user);
   setTableItem("users", "username", $user, $item, $value);
}

function addCash($amt){
   $user = getUser();
   setTableItem("users", "username", $user, "cash", intval(getUserItem($user, "cash")) + intval($amt) );
}

function addItem($itemToAdd){
   $user = getUser();
   for ($i = 1; $i <= 5; $i ++){
      if (getUserItem($user, "item$i") == "empty.png"){
         setUserItem($user, "item$i", $itemToAdd);
         return $i;
      }
   }
   return False;
}

function getUserItem($user, $item){
   $user = strtolower($user);
   return getTableItem("users", "username", $user, $item);
}

function getKeyIntItem($key){
   return getTableItem("IntMap", "key", $key, "value");
}

function setKeyIntItem($key, $value){
   return setTableItem("IntMap", "key", $key, "value", $value);
}

function isTakenInTable($table, $category, $value){
   $sql = sprintf('SELECT %s FROM %s', mysql_escape_string($category), mysql_escape_string($table));
   $result = mysql_query($sql);
   $numRows =  mysql_num_rows($result);
   for ($i = 0; $i < $numRows; $i++){
     $row = mysql_fetch_assoc($result);
     if ( $value == $row[$category] ){
       return TRUE;
     }
   }
   return FALSE;
}

function newKeyIntItem($key, $value){
   $key = mysql_real_escape_string($key);
   $value = mysql_real_escape_string($value);
   if (isTakenInTable("IntMap", "key", $key)){
      return FALSE;
   } else {
      $msg = "INSERT INTO `new`.`IntMap` (`key`, `value`) VALUES ('$key', '$value');";
      $sql = sprintf($msg);
      $toReturn = mysql_query($sql);
      return $toReturn;
   }
}

function showItem($itemN){
   $itemLoc = getUserItem(getUser(), $itemN);

   if ($itemLoc == "empty.png"){
      echo "<center><font class='userpanelname'>EMPTY</font></center>";
   }
   
   if ($itemLoc != "empty.png"){
      echo "<img src='./items/", $itemLoc, "' alt='", $itemN, "'";
      echo " />";
   }
}

function isTaken($category, $value){
   $sql = sprintf('SELECT %s FROM users', mysql_escape_string($category));
   $result = mysql_query($sql);
   $numRows =  mysql_num_rows($result);
   for ($i = 0; $i < $numRows; $i++){
     $row = mysql_fetch_assoc($result);
     if (strtolower($value) == $row[$category]){
       return TRUE;
     }
   }
   return FALSE;
}

function login($user, $pass){
   if (strrpos($user, '@') === FALSE){
      $password = getUserItem($user, "password");
      if (isEmptyString($password)){
         return FALSE;
      } else {
         if ($pass == $password){
            setAuthenticated(TRUE);
            setUser($user);
            return TRUE;
         } else {
            return FALSE;
         }
      }
   } else {
      $sql = sprintf('SELECT username, email FROM users');
      $result = mysql_query($sql);
      $numRows =  mysql_num_rows($result);
      for ($i = 0; $i < $numRows; $i++){
         $row = mysql_fetch_assoc($result);
         $curruser = $row["username"];
         if (strtolower($user) == $row["email"]){
            $password = getUserItem($curruser, "password");
            if ($pass == $password){
               setAuthenticated(TRUE);
               setUser($curruser);
               return TRUE;
            }
         }
      }
      return FALSE;     
   }
}

function getEmailUser($email){
   $sql = sprintf('SELECT username, email FROM users');
   $result = mysql_query($sql);
   $numRows =  mysql_num_rows($result);
   for ($i = 0; $i < $numRows; $i++){
      $row = mysql_fetch_assoc($result);
      $curruser = $row["username"];
      if (strtolower(htmlspecialchars($email)) == $row["email"]){
         return $curruser;
      }
   }
   return FALSE;    
}

function isVerified($user){
   if (!isTaken('username', $user)){
      return FALSE;
   }
   if (getUserItem($user, "status") == "varpend"){
      return FALSE;
   }
   return TRUE;
}

function updateLastLogin($user){
   $sql = sprintf('UPDATE `savehoa`.`users` SET `lastlogin` = CURRENT_TIMESTAMP WHERE CONVERT(`users`.`username` USING utf8) = \'%s\' LIMIT 1;', 
      $user);
   return mysql_query($sql);
}

function updatePageViews(){
   $pageName = substr($_SERVER["PHP_SELF"], 1);
   if (isEmptyString($pageName)){
      $pageName = 'index.php';
   }
   if ( isTakenInTable("PageViews", "pagename", $pageName) ){
      $newTotalViews = getTableItem("PageViews", "pagename", $pageName, "totalviews") + 1;
      setTableItem("PageViews", "pagename", $pageName, "totalviews", $newTotalViews);
      $sql = sprintf('UPDATE `savehoa`.`PageViews` SET `lastview` = CURRENT_TIMESTAMP WHERE CONVERT(`PageViews`.`pagename` USING utf8) = \'%s\' LIMIT 1;', 
      $pageName);
      mysql_query($sql);
      if (isAuthenticated()){
        $newUserViews = getTableItem("PageViews", "pagename", $pageName, "userviews") + 1;
        setTableItem("PageViews", "pagename", $pageName, "userviews", $newUserViews);
      } else {
        $newNonuserViews = getTableItem("PageViews", "pagename", $pageName, "nonuserviews") + 1;        
        setTableItem("PageViews", "pagename", $pageName, "nonuserviews", $newNonuserViews);
      }
   } else { // Create a new page server
      $msg = "INSERT INTO `savehoa`.`PageViews` (`pagename`, `totalviews`, `nonuserviews`, `userviews`, `lastview`) VALUES ('$pageName','0','0', '0', CURRENT_TIMESTAMP)";
      $sql = sprintf($msg);
      mysql_query($sql);
   }
   if (isAuthenticated()){
      $newUserViews = getUserItem(getUser(), "pageviews") + 1;
      setUserItem(getUser(), "pageviews", $newUserViews);
   }
}

function hasFlagged($user, $table, $post){
   $sql = sprintf("SELECT * FROM `commentflags` WHERE `flagger`='$user' AND `table`='$table' AND `post`='$post';");
   $result = mysql_query($sql);
   if ( mysql_num_rows($result) === 1 ){
      return TRUE;
   }
   return FALSE;
}

function commentOutOfBounds($table, $postnumber){
   if (isTakenInTable("conversations", "tableid", $table)){
      if (isTakenInTable("ztable" . $table, "postnumber", $postnumber) ){
         return FALSE;
      }
   }
   return TRUE;
}

function colorAlert($alert, $color){
   openBox(800,10,"middle",$color);
     echo "<center><font size='8'>$alert</font></center>";
   closeBox($color);
}

function addImageButton($id, $staticimg, $hoverimg, $clickimg, $link){
   echo "<a href='$link' onmouseout=\"swapImage('$id','$staticimg')\" onmouseover=\"swapImage('$id','$hoverimg')\" ";
   echo "onmousedown=\"swapImage('$id','$clickimg')\"  >";
   echo "<img id='$id' src='$staticimg' />";
   echo "</a>";
}

function addImageButtonWithTitle($id, $staticimg, $hoverimg, $clickimg, $link, $titleOfImg){
   echo "<a href='$link' onmouseout=\"swapImage('$id','$staticimg')\" onmouseover=\"swapImage('$id','$hoverimg')\" ";
   echo "onmousedown=\"swapImage('$id','$clickimg')\"  >";
   echo "<img id='$id' src='$staticimg' title='$titleOfImg' alt='$titleOfImg' />";
   echo "</a>";
}

function addFormatButton($id, $staticimg, $hoverimg, $clickimg, $format, $titleOfImg){
   echo "<img id='$id' class='formatbutton' title='$titleOfImg' src='$staticimg' ";
   echo "onmouseout=\"swapImage('$id','$staticimg')\" ";    
   echo "onmouseover=\"swapImage('$id','$hoverimg')\" onclick=\"addText('$format')\" ";
   echo "onmousedown=\"swapImage('$id','$clickimg')\"  />";
}

function addSeparator(){
   echo "<br /><img src='img/separator2.png'><br />";
}

function addFiller($width, $height){
   echo "<table height='$height'><tr><td width='$width'></td></tr></table>";
}

function getBrowser(){ 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }  
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    return $bname;
}

function openBox($width, $height, $valign, $color){
   $alt = '';
   if ($color != 'gray'){
      $alt = $color;
   }
   echo "<table class='roundedgraybox' width='$width' height='$height'>";
     echo "<tr height='11'>";
       echo "<td width='11'><img src='img/transparent/$alt","transtopleft.png' /></td>";
       echo "<td class='filler$color' id='t$id'></td>";
       echo "<td width='11'><img src='img/transparent/$alt","transtopright.png' /></td>";
     echo "</tr>";
     echo "<tr>";
       echo "<td class='filler$color'></td>";
       echo "<td class='filler$color' valign='$valign'>";  
}

function sqlhtmlsafe($toMakeSafe){
   $safeString = "";
   for ($i = 0; $i < strlen($toMakeSafe); $i ++){
      $char = substr($toMakeSafe, $i, 1);
      if ($char == '\\'){
         
      } else {
         $safeString .= $char;
      }
   }
   return htmlspecialchars($safeString);
}

function openLinkBox($width, $height, $valign, $color, $id, $link){
   echo "<a href='$link'>";
   $alt = '';
   if ($color != 'gray'){
      $alt = $color;
   }
   echo "<table class='roundedgraybox' width='$width' height='$height' onmouseover=\"highlight('$id', 'red')\" onmouseout=\"highlight('$id', '$color')\">";
     echo "<tr height='11'>";
       echo "<td width='11'><img src='img/transparent/$alt","transtopleft.png' id='tl$id' /></td>";
       echo "<td class='filler$color' id='t$id'></td>";
       echo "<td width='11'><img src='img/transparent/$alt","transtopright.png' id='tr$id' /></td>";
     echo "</tr>";
     echo "<tr valign='$valign'>";
       echo "<td class='filler$color' id='l$id'></td>";
       echo "<td class='filler$color' valign='$valign' id='m$id'>";  
}

function closeLinkBox($color, $id){
   $alt = '';
   if ($color != 'gray'){
      $alt = $color;
   }
   echo "</td>";
       echo "<td class='filler$color' id='r$id'></td>";
     echo "</tr>";
     echo "<tr height='11'>";
       echo "<td width='11'><img src='img/transparent/$alt","transbotleft.png' id='bl$id' /></td>";
       echo "<td class='filler$color' id='b$id'></td>";
       echo "<td width='11'><img src='img/transparent/$alt","transbotright.png' id='br$id' /></td>";
     echo "</tr>";
   echo "</table></a>";
}

function closeBox($color){
   $alt = '';
   if ($color != 'gray'){
      $alt = $color;
   }
   echo "</td>";
       echo "<td class='filler$color'></td>";
     echo "</tr>";
     echo "<tr height='11'>";
       echo "<td width='11'><img src='img/transparent/$alt","transbotleft.png' /></td>";
       echo "<td class='filler$color' id='b$id'></td>";
       echo "<td width='11'><img src='img/transparent/$alt","transbotright.png' /></td>";
     echo "</tr>";
   echo "</table>";
}

function addCurvedTitle($title, $width, $color){
  openBox($width,10, "middle", $color);
    echo "<center><font size='8'>$title</font></center>";
  closeBox($color);
}

?>
