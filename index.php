<?php include 'library.php' ?>

<?php

/**
 * @author Hoa Mai
 * @copyright 2014
 */
/*
session_start();

connect();
*/
?>
<!DOCTYPE html>
<html>
 <head>
  <title>Triton Cloud Manager</title>
  
  <link rel="icon" type="image/png" href="tritoncloud.png"/>
  <link rel='stylesheet' href='common.css' />
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
  <script src="http://www.flotcharts.org/javascript/jquery.flot.min.js"></script>
  <script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
  
<style>

  body {
    margin:0px;
    font-family:verdana;
    font-size:small;
  }
  
  .circleButton {
    width: 300px;
    opacity:.5;
    border-radius:300px;
    font-size:xx-large;
    color:white;
    text-align:center;
  }
  
  .page {
    top:100px;
    left:0px;
    height:500px;
    width:100%;
  }
  
  .textfield {
    width:380px;
    height:30px;
  }
  
  .textarea {
    width:380px;
    height:150px;
  }
  
  .testButton {
    background-color:#BBBBBB;
    width:380px;
    height:50px;
    font-size:medium;
    font-weight:900;
    color:white;
  }
  
  .testButtonHL {
    background-color:#666666;
    width:380px;
    height:50px;
    font-size:medium;
    font-weight:900;
    color:white;
  }
  
  .graph {
    width:280px;
    height:135px;
  }
  
  .box {
    width:285px;
    height:170px;
    border:1px solid black;
    font-size:x-small;
  }
  
  #returnButton {
    background-color:#33B5DB;
    color:white;
    height:50px;
    width:250px;
    font-size:medium;
    font-weight:900;
  }

</style>
<script>

  var Content = function() { }; 
  Content.svgWidth = 0;
  Content.svgHeight = 0;
  Content.pages = ['h1page', 'b1page', 'b2page', 'b3page'];
  Content.currPage = 'h1page';
  Content.newPage = 'h1page';
  Content.currCase = 'functionality';
  Content.increase = 0;
  Content.count = 0;
  
  Content.curr = 0;
  Content.currI = 0;
  
  Content.currS = [];
  Content.currS1 = [];
  Content.currS2 = [];
  
  Content.currS3 = [];
  Content.currS4 = [];
  
  Content.currLB = [];
  Content.currMC = [];
  Content.currDB = [];
  Content.currS[0] = [[0,10]];
  Content.currS1[0] = [[0,12]];
  Content.currS2[0] = [[0,11]];
  
  Content.currS3[0] = [[0,44]];
  Content.currS4[0] = [[0,38]];
  
  Content.currLB[0] = [[0,17]];
  Content.currMC[0] = [[0,22]];
  Content.currDB[0] = [[0,19]];
  
  window.onload = start;
  setInterval("updateMonitor()",5000);

  $(function(){
    $(window).resize(function() {
      refresh();
    });
    // JQuery Methods area
  });
  
  function refresh() {
    
  }
  
  function getRandom(start, end){
    var diff = end - start;
    return Math.floor( Math.random() * diff ) + start;
  }
  
  function start() {
    var data = [];
    var curr = 20;
    var diff = 1;
    data[0] = [[1,20]];
    
    /*
    for (var i = 1; i < 100; i++){
      curr = getRandom( curr - diff, curr + diff + 1);
      data[i] = [data[i - 1][1],[ i, curr]];
    }
    */
    $.plot($("#lb1"), data, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
    
    $.plot($("#s1"), data, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
    $.plot($("#s2"), data, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
    $.plot($("#s3"), data, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
    $.plot($("#s4"), data, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
    $.plot($("#s5"), data, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
    
    $.plot($("#mc1"), data, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
    $.plot($("#db1"), data, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
    
    for (var i = 0; i < Content.pages.length; i++) {
        $('#' + Content.pages[i]).hide();
    }
    showPage('h1page');
    refresh();
  }
  
  function hl(id) {
    $( "#" + id ).animate({
        opacity: 0.999,
        }, 400, function() {
            // Animation complete.
        }
    );
  }
  
  function dl(id) {
    $( "#" + id ).animate({
        opacity: 0.5,
        }, 400, function() {
            // Animation complete.
        }
    );
  }
  
  function cl(id) {
    showPage(id + 'page');
  }
  
  function back() {
    showPage('h1page');
  }
  
  function showPage(page) {
    Content.newPage = page;
    $('#' + Content.currPage).animate({
        opacity: 0,
    }, 400, function() {
        $('#' + Content.currPage).hide();
        Content.currPage = Content.newPage;
        $('#' + Content.currPage).css('opacity', '0');
        $('#' + Content.currPage).show();
        $('#' + Content.currPage).animate({opacity:0.999}, 400, function() {});
        runPage();
    });
    refresh();
  }
  
  function runPage() {
    if (Content.currPage == 'b1page') {
        
    } else if (Content.currPage == 'b2page') {
        
    } else if (Content.currPage == 'b3page') {
        $.get("updateall.php");
    }
  }
  
  function updateMonitor() {
    if (Content.currPage == 'b1page') {
        $('#dbs1').html(getRandom(25, 35));
        $('#ss1').html(getRandom(20 + Content.increase / 2, 30 + Content.increase / 2));
        $('#ss2').html(getRandom(20 + Content.increase / 2, 30 + Content.increase / 2));
        $('#ss3').html(getRandom(20 + Content.increase / 2, 30 + Content.increase / 2));
        $('#mcs1').html(getRandom(25, 35));
        $('#lbs1').html(getRandom(25, 35));
       
        Content.currI ++;
        
        if (Content.count == 12) {
            $('#hide').show();
            Content.increase = 35;
        }
        
        Content.currLB[0][Content.currI] = [Content.currI,getRandom(15,25)];
        Content.currMC[0][Content.currI] = [Content.currI,getRandom(15,25)];
        Content.currDB[0][Content.currI] = [Content.currI,getRandom(15,25)];
        Content.currS[0][Content.currI] = [Content.currI,getRandom(10 + Content.increase,20 + Content.increase)];
        Content.currS1[0][Content.currI] = [Content.currI,getRandom(10 + Content.increase,20 + Content.increase)];
        Content.currS2[0][Content.currI] = [Content.currI,getRandom(10 + Content.increase,20 + Content.increase)];
        
        if (Content.increase == 35) {
            
            $('#ss4').html(getRandom(20 + Content.increase / 2, 30 + Content.increase / 2));
            $('#ss5').html(getRandom(20 + Content.increase / 2, 30 + Content.increase / 2));
            
            Content.currS3[0][Content.currI] = [Content.currI,getRandom(10 + Content.increase,20 + Content.increase)];
            Content.currS4[0][Content.currI] = [Content.currI,getRandom(10 + Content.increase,20 + Content.increase)];
            
            $.plot($("#s4"), Content.currS3, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
            $.plot($("#s5"), Content.currS4, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
            
        }
        
        $.plot($("#lb1"), Content.currLB, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
        
        $.plot($("#s1"), Content.currS, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
        $.plot($("#s2"), Content.currS1, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
        $.plot($("#s3"), Content.currS2, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
        
        $.plot($("#mc1"), Content.currMC, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
        $.plot($("#db1"), Content.currDB, { yaxis: { max: 100, min:0 }, xaxis: { min: 0, max:100 } });
        
        if (Content.increase > 20) {
            Content.count ++;
        }
        
    }
  }
  
  function test() {
    $.get('loadtest.php');
    Content.increase = 70;
    showPage('b1page');
  }
  
  function hlTest(id) {
    $('#' + Content.currCase).removeClass('testButtonHL');
    $('#' + Content.currCase).addClass('testButton');
    Content.currCase = id;
    $('#' + Content.currCase).removeClass('testButton');
    $('#' + Content.currCase).addClass('testButtonHL');
  }
  
  function clearTest() {
    $('#t1').val("");
    $('#t2').val("");
    $('#t3').val("");
    $('#t4').val("");
    
    
  }
  
  function addTestButton(id) {
    $('#testoptions').append("<br /><table id='" + id + "' class='curve5 clickable center testButton' onclick=\"hlTest('" + id + "');\"><tr><td class='center'>" + id + "</td></tr></table>")
  }
  
  
  
  function addTest() {
    addTestButton($('#t1').val());
    clearTest();
  }

</script>
  
 </head>
 <body>
 
  <div class='backgroundImage'>
   <img src='wallpaper.png' 
     width='100%' height='100%' />
  </div>
  
  <table id='h1' class="full blackBackground clickable" 
        style="height: 100px;opacity:.5;" onclick="back()">
   <tr>
    <td  style='vertical-align:center;font-size:xx-large;color:white;padding-left:20px;' >
     Triton Cloud Manager
    </td>
   </tr>
  </table>
  
  <table id="mainnavbody" class="full" style="height:550px;opacity:.9999">
   <tr>
    <td style='vertical-align:center;font-size:xx-large;color:white;'>

     <table class="full" style="height:300px;">
      <tr>
       <td></td>
       <td id='b1' class='blackBackground clickable circleButton' onmouseover="hl('b1');" onmouseout="dl('b1')" onclick="cl('b1')">
        Monitor Servers
       </td>
       <td></td>
       <td id="b2" class='blackBackground clickable circleButton' onmouseover="hl('b2');" onmouseout="dl('b2')" onclick="cl('b2')">
        Test Servers
       </td>
       <td></td>
       <td id="b3" class='blackBackground clickable circleButton' onmouseover="hl('b3');" onmouseout="dl('b3')" onclick="cl('b3')">
        Update All Servers
       </td>
       <td></td>
      </tr>
     </table>
    
    
    </td>
   </tr>
  </table>
  
  <div id="h1page">
  </div>
  
  <table id='b1page' class="modal whiteBackground center page" style="font-size: large;">
   <tr>
    <td style="vertical-align: middle;">
    
    <table class="full" style="height: 420;">
     <tr>
      <td></td>
      <td style="width: 300px;color: #00EE00;">
       Load Balancers
      </td>
      <td></td>
      <td style="width: 300px;color:blue">
       Servers
      </td>
      <td></td>
      <td style="width: 300px;color:orange">
       Memcaches
      </td>
      <td></td>
      <td style="width: 300px;color:#DD0000">
       Databases
      </td>
      <td></td>
     </tr>
     <tr style="height: 350px;">
      <td></td>
      <td class='curve5 center' style="border: 1px solid black;">
      <div style="width: 100%;height:345px;overflow-y:scroll;">
      
      <div class="box">
       CPU Usage:
       <div id="lb1" class='graph'></div>
       Status: <span style="color: #00DD00;">Running</span>
       Memory Usage: <span id='lbs1' style="color: #00DD00;">30</span>%
      </div>
      
      </div>
      
      </td>
      <td></td>
      <td class='curve5 center' style="border: 1px solid black;">
      <div id='serverdiv' style="width: 100%;height:345px;overflow-y:scroll;">
      
      <div class="box" title="54.219.215.235">
       CPU Usage:
       <div id="s1" class='graph'></div>
       Status: <span style="color: #00DD00;">Running</span>
       Memory Usage: <span id='ss1' style="color: #00DD00;">30</span>%
      </div>
      
      <div class="box" title="54.219.59.85">
       CPU Usage:
       <div id="s2" class='graph'></div>
       Status: <span style="color: #00DD00;">Running</span>
       Memory Usage: <span id='ss2' style="color: #00DD00;">30</span>%
      </div>
      
      <div class="box" title="54.219.204.176">
       CPU Usage:
       <div id="s3" class='graph'></div>
       Status: <span style="color: #00DD00;">Running</span>
       Memory Usage: <span id='ss3' style="color: #00DD00;">30</span>%
      </div>
      
      <div id="hide" style="display: none;">
      
      <div class="box">
       CPU Usage:
       <div id="s4" class='graph'></div>
       Status: <span style="color: #00DD00;">Running</span>
       Memory Usage: <span id='ss4' style="color: #00DD00;">30</span>%
      </div>
      
      <div class="box">
       CPU Usage:
       <div id="s5" class='graph'></div>
       Status: <span style="color: #00DD00;">Running</span>
       Memory Usage: <span id='ss5' style="color: #00DD00;">30</span>%
      </div>
      
      </div>
      
      </div>
      </td>
      <td></td>
      <td class='curve5 center' style="border: 1px solid black;">
      <div style="width: 100%;height:345px;overflow-y:scroll;">
      
      <div class="box">
       CPU Usage:
       <div id="mc1" class='graph'></div>
       Status: <span style="color: #00DD00;">Running</span>
       Memory Usage: <span id='mcs1' style="color: #00DD00;">30</span>%
      </div>
      
      </div>
      </td>
      <td></td>
      <td class='curve5 center' style="border: 1px solid black;">
      <div style="width: 100%;height:345px;overflow-y:scroll;">
      
      <div class="box">
       CPU Usage:
       <div id="db1" class='graph'></div>
       Status: <span style="color: #00DD00;">Running</span>
       Memory Usage: <span id='dbs1' style="color: #00DD00;">30</span>%
      </div>
      
      </div>
      
      </td>
      <td></td>
     </tr>
    </table>
    <br />
    <table id='returnButton' class='curve5 clickable center' onclick="back();">
        <tr>
        <td class='center'>
        Back
        </td>
        </tr>
    </table>
    
    </td>
   </tr>
  </table>
  
  <table id='b2page' class="modal whiteBackground center page">
   <tr>
    <td style="vertical-align: middle;">
     
     <table class="full" style="height: 450px;">
      <tr>
       <td></td>
       <td id='testoptions' style="width: 400px;">
       
       <table id='functionality' class='curve5 clickable center testButton' onclick="hlTest('functionality');">
        <tr>
        <td class='center'>
         Check Functionality
        </td>
        </tr>
       </table>
       <br />
       <table id='stress' class='curve5 clickable center testButton' onclick="hlTest('stress');">
        <tr>
        <td class='center'>
         Stress Test
        </td>
        </tr>
       </table>
       
       </td>
       <td></td>
       <td style="width: 400px;font-size:medium;font-weight:900;">
       
       Test Case Name:<br />
       <input id="t1" type="text" class='textfield'/><br />
       Requests Per Second:<br />
       <input id="t2" type="text" class='textfield' /><br />
       Test Case Duration:<br />
       <input id="t3" type="text" class='textfield'/><br />
       Test Case Pattern:<br />
       <textarea id="t4" class='textarea'></textarea>
       
       </td>
       <td></td>
      </tr>
      <tr style="height: 50px;">
       <td></td>
       <td class="clickable curve5 center cyanButton" style="background-color: #00DD00;" onclick="test()">
        Run Test
       </td>
       <td></td>
       <td class="clickable curve5 center cyanButton" style="background-color: orange;" onclick="addTest()">
        Create Test
       </td>
       <td></td>
      </tr>
     </table>
    
    
    
    </td>
   </tr>
  </table>
  
  <table id='b3page' class="modal whiteBackground center page">
   <tr>
    <td style="vertical-align: middle;">
    
    <h1>
    All of your servers has been <span style='color:#00DD00;'>successfully</span> updated!
    </h1>
    <br />
    <br />
    
    <table id='returnButton' class='curve5 clickable center' onclick="back();">
        <tr>
        <td class='center'>
        Back
        </td>
        </tr>
    </table>
    
    <br />
    <br />
    
    </td>
   </tr>
  </table>
 
 
 </body>
</html>
